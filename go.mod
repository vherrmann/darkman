module gitlab.com/WhyNotHugo/darkman

go 1.16

require (
	github.com/adrg/xdg v0.3.3
	github.com/godbus/dbus/v5 v5.0.4
	github.com/integrii/flaggy v1.5.2
	github.com/kr/pretty v0.2.0 // indirect
	github.com/sj14/astral v0.1.2
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
